import axios from '../../axios-api';

export const FETCH_SHORT_URL_SUCCESS = 'FETCH_SHORT_URL_SUCCESS';
export const FETCH_REDIRECT_SUCCESS = 'FETCH_REDIRECT_SUCCESS';

export const fetchShortUrlSuccess = data => {
  return {type: FETCH_SHORT_URL_SUCCESS, data};
};

export const getShortUrl = link => {
  return dispatch => {
    return axios.post(`/links`, link).then(
      response => dispatch(fetchShortUrlSuccess(response.data))
    )
  }
};
