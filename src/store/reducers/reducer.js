import {FETCH_SHORT_URL_SUCCESS} from "../actions/actions";

const initialState = {
  link: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SHORT_URL_SUCCESS:
      return {...state, link: action.data};
    default:
      return state;
  }
};

export default reducer;