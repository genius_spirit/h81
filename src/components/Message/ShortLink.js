import React from 'react';
import {Message} from "semantic-ui-react";

const ShortLink = props => {
  return (
    <Message compact style={{marginTop: '30px'}}>
      <Message.Header>You link now look like this:</Message.Header>
      <p>
        <a href={`http://localhost:8000/${props.shortUrl}`}
           style={{cursor: 'pointer'}}>
          {`http://localhost:8000/${props.shortUrl}`}
        </a>
      </p>
    </Message>
  );
};

export default ShortLink;

