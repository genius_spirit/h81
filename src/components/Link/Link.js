import React, {Component} from 'react';
import {Button, Container, Form, Header, Input} from "semantic-ui-react";
import {connect} from 'react-redux';
import {getShortUrl} from "../../store/actions/actions";
import ShortLink from "../Message/ShortLink";

class Link extends Component {

  state = {
    url: '',
    show: false
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.onCreateShortUrl({originalUrl: this.state.url});
    this.setState({
      show: true
    });
  };

  render() {
    return (
      <Container textAlign='center'>
        <Header style={{marginTop: '150px', marginBottom: '30px', fontSize: '30px'}}>Shorten your link!</Header>
        <Form onSubmit={this.submitFormHandler}>
          <Form.Field>
            <Input focus placeholder='Enter URL here'
                   style={{width: '600px'}}
                   name="url"
                   value={this.state.url}
                   onChange={this.inputChangeHandler}
            />
          </Form.Field>
          <Form.Field>
            <Button primary type='submit'>Shorten!</Button>
          </Form.Field>
        </Form>
        {this.state.show
          ? <ShortLink shortUrl={this.props.link.shortUrl}
                     originalUrl={this.props.link.originalUrl} />
          : null}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    link: state.link
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCreateShortUrl: link => dispatch(getShortUrl(link))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Link);
